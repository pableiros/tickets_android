package com.pableiros.tickets.model.retrofit

import com.pableiros.tickets.BuildConfig
import com.pableiros.tickets.common.network.NetworkService

class TicketNetworkService: NetworkService<TicketApiInterface>(
    BuildConfig.url,
    TicketApiInterface::class.java,
    arrayListOf()
)