package com.pableiros.tickets.model.local

import com.pableiros.tickets.model.entities.Modulo
import com.pableiros.tickets.model.entities.ModuloRealm
import io.realm.Realm

object ModuloLocalData {

    fun store(modulos: List<Modulo>) {
        val realm = Realm.getDefaultInstance()

        realm.executeTransaction { bgRealm ->
            bgRealm.where(ModuloRealm::class.java).findAll().deleteAllFromRealm()

            val modulosRealm = modulos.asSequence().map { ModuloRealm.create(it, bgRealm) }.toList()
            bgRealm.insert(modulosRealm)
        }

        realm.close()
    }

    fun getAll(): List<Modulo> {
        val realm = Realm.getDefaultInstance()
        val modulosRealm = realm.where(ModuloRealm::class.java).findAll()
        val modulos = if (modulosRealm.count() > 0) {
            modulosRealm.map { it.toModulo() }
        } else {
            val modulos = mutableListOf<Modulo>()
            modulos.add(Modulo.createComprasInstance())
            modulos.add(Modulo.createGastosViajeInstance())
            modulos.add(Modulo.createMantenimientoInstance())
            modulos.add(Modulo.createProcesosInstance())
            modulos.add(Modulo.createRecursosHumanosInstance())
            modulos
        }

        realm.close()

        return modulos
    }
}