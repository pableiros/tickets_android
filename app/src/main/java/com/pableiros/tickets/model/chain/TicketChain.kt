package com.pableiros.tickets.model.chain

import com.pableiros.tickets.model.entities.Ticket
import com.pableiros.tickets.model.repository.TicketRepository

class TicketChain {

    var ticket: Ticket? = null
    var nextChain: TicketChain? = null

    fun handle(completionHandler: ((Boolean) -> Unit)) {
        val ticketRepository = TicketRepository()
        ticketRepository.create(ticket!!) { success ->
            if (!success) {
                nextChain = null
            }

            dispatchNextChain(completionHandler)
        }
    }

    private fun dispatchNextChain(completionHandler: ((Boolean) -> Unit)) {
        if (nextChain != null) {
            nextChain?.handle(completionHandler)
        } else {
            completionHandler(true)
        }
    }

    companion object {

        fun createChain(tickets: List<Ticket>): TicketChain? {
            val ticketsReversed = tickets.reversed()
            var ticketNextChain: TicketChain? = null

            ticketsReversed.forEach { ticket ->
                val existingLink = ticketNextChain
                val ticketChain = TicketChain()

                ticketChain.ticket = ticket
                ticketNextChain = ticketChain
                ticketNextChain?.nextChain = existingLink
            }

            return ticketNextChain
        }
    }
}