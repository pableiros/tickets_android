package com.pableiros.tickets.model.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Ticket(
    @SerializedName("id")
    var id: String?,
    @SerializedName("fecha_inicio")
    var fechaInicio: String?,
    @SerializedName("fecha_fin")
    var fechaFin: String?,
    @SerializedName("folio")
    var folio: String?,
    @SerializedName("descripcion")
    var descripcion: String?,
    @SerializedName("estatus")
    var estatus: String?,
    @SerializedName("modulo_id")
    var moduloId: String?,
    @SerializedName("modulo_nombre")
    var moduloNombre: String?,
    @SerializedName("sync_id")
    var syncId: String?
): Parcelable {

    var modulo: Modulo?
        get() {
            var modulo: Modulo? = null

            if (moduloId != null && moduloNombre != null) {
                modulo = Modulo(moduloId, moduloNombre)
            }

            return modulo
        }
        set(value) {
            moduloId = value?.id
            moduloNombre = value?.nombre
        }

    fun toTicketCreationRequestData(): TicketCreationRequestData {
        return TicketCreationRequestData(fechaInicio, descripcion, moduloId, syncId)
    }

    private constructor(p: Parcel?): this(
            id = p?.readString(),
            fechaInicio = p?.readString(),
            fechaFin = p?.readString(),
            folio = p?.readString(),
            descripcion = p?.readString(),
            estatus = p?.readString(),
            moduloId = p?.readString(),
            moduloNombre = p?.readString(),
            syncId = p?.readString()
    )

    override fun toString(): String {
        return descripcion ?: ""
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        p0?.writeString(id)
        p0?.writeString(fechaInicio)
        p0?.writeString(fechaFin)
        p0?.writeString(folio)
        p0?.writeString(descripcion)
        p0?.writeString(estatus)
        p0?.writeString(moduloId)
        p0?.writeString(moduloNombre)
        p0?.writeString(syncId)
    }

    override fun describeContents() = 0

    companion object {
        @JvmField val CREATOR = object : Parcelable.Creator<Ticket> {
            override fun createFromParcel(p0: Parcel?) = Ticket(p0)

            override fun newArray(p0: Int) = arrayOfNulls<Ticket>(p0)
        }
    }
}