package com.pableiros.tickets.model.entities

import com.google.gson.annotations.SerializedName

class TicketCreationResponseData(
        @SerializedName("id")
        var id: String? = null,
        @SerializedName("fecha_inicio")
        var fechaInicio: FechaInicio? = null,
        @SerializedName("descripcion")
        var descripcion: String? = null,
        @SerializedName("estatus")
        var estatus: String? = null,
        @SerializedName("folio")
        var folio: String? = null,
        @SerializedName("modulo_id")
        var moduloId: String? = null,
        @SerializedName("sync_id")
        var syncId: String? = null
) {

    inner class FechaInicio(
            @SerializedName("date")
            var fecha: String?,
            @SerializedName("timezone_type")
            var timezoneType: String?,
            @SerializedName("timezone")
            var timezone: String?
    )

    fun toTicket(): Ticket {
        return Ticket(
                id,
                fechaInicio?.fecha,
                null,
                folio,
                descripcion,
                estatus,
                moduloId,
                null,
                syncId
        )
    }
}