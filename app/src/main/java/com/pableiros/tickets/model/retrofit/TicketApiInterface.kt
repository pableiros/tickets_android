package com.pableiros.tickets.model.retrofit

import com.pableiros.tickets.model.entities.Modulo
import com.pableiros.tickets.model.entities.Ticket
import com.pableiros.tickets.model.entities.TicketCreationRequestData
import com.pableiros.tickets.model.entities.TicketCreationResponseData
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface TicketApiInterface {

    @GET("/api/v1/ticket")
    fun getTickets(): Single<List<Ticket>>

    @GET("/api/v1/modulos")
    fun getModulos(): Single<List<Modulo>>

    @POST("/api/v1/ticket")
    fun createTicket(@Body ticketCreationRequestData: TicketCreationRequestData): Single<TicketCreationResponseData>
}