package com.pableiros.tickets.model.repository

import com.pableiros.tickets.common.*
import com.pableiros.tickets.common.singleton.RxApplication
import com.pableiros.tickets.model.entities.Modulo
import com.pableiros.tickets.model.local.ModuloLocalData
import com.pableiros.tickets.model.remote.ModuloRemoteData
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

class ModuloRepository: BaseRepository<List<Modulo>>(RxApplication.getRequestCache()) {

    override val cacheKey: String
        get() = "ModuloRepository"

    override var outcome: PublishSubject<Outcome<List<Modulo>>>?
        get() = modulosOutcome
        set(value) {}

    private var modulosOutcome: PublishSubject<Outcome<List<Modulo>>> = PublishSubject.create()

    fun fetch() {
        modulosOutcome.loading(true)
        performLocalRequest(getCachedObservable())
    }

    fun refresh() {
        modulosOutcome.loading(false)
        performLocalRequest(buildFreshObservable())
    }

    override fun buildFreshObservable(): Single<List<Modulo>> {
        val moduloRemoteData = ModuloRemoteData()
        val observable = moduloRemoteData.getModulos().cache()
        responseCache.put(cacheKey, observable)
        return observable
    }

    override fun performLocalRequest(observable: Single<List<Modulo>>) {
        observable.performOnBackOutOnMain(scheduler)
                .subscribe ({
                    ModuloLocalData.store(it)
                    outcome?.success(it)
                }, {
                    val modulos = ModuloLocalData.getAll()
                    outcome?.success(modulos)
                })
                .addTo(compositeDisposable)
    }
}