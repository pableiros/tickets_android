package com.pableiros.tickets.model.entities

import com.google.gson.annotations.SerializedName

class TicketCreationRequestData(
        @SerializedName("fecha_inicio")
        var fechaInicio: String?,
        @SerializedName("descripcion")
        var descripcion: String?,
        @SerializedName("modulo_id")
        var moduloId: String?,
        @SerializedName("sync_id")
        var syncId: String?
)