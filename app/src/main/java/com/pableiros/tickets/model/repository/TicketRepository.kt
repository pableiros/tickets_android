package com.pableiros.tickets.model.repository

import com.pableiros.tickets.common.*
import com.pableiros.tickets.common.singleton.RxApplication
import com.pableiros.tickets.model.entities.Ticket
import com.pableiros.tickets.model.entities.TicketCreationResponseData
import com.pableiros.tickets.model.local.TicketLocalData
import com.pableiros.tickets.model.remote.TicketRemoteData
import com.pableiros.tickets.presentation.ticketform.TicketFormFragment
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import io.reactivex.functions.Consumer

class TicketRepository: BaseRepository<List<Ticket>>(RxApplication.getRequestCache()) {

    override val cacheKey: String
        get() = "TicketRepository"

    override var outcome: PublishSubject<Outcome<List<Ticket>>>?
        get() = ticketOutcome
        set(@Suppress("UNUSED_PARAMETER") value) {}

    private var ticketOutcome: PublishSubject<Outcome<List<Ticket>>> = PublishSubject.create()

    var ticketFormOutcome: PublishSubject<Outcome<TicketCreationResponseData>> = PublishSubject.create()

    fun fetch() {
        ticketOutcome.loading(true)
        performLocalRequest(getCachedObservable())
    }

    fun refresh(showLoading: Boolean) {
        ticketOutcome.loading(showLoading)
        performLocalRequest(buildFreshObservable())
    }

    override fun buildFreshObservable(): Single<List<Ticket>> {
        val ticketRemoteData = TicketRemoteData()
        val observable = ticketRemoteData.getTickets().cache()
        responseCache.put(cacheKey, observable)
        return observable
    }

    override fun performLocalRequest(observable: Single<List<Ticket>>) {
        observable.performOnBackOutOnMain(scheduler)
                .subscribe ({
                    TicketLocalData.store(it)
                    val tickets = TicketLocalData.getAll()
                    outcome?.success(tickets)
                }, {
                    val tickets = TicketLocalData.getAll()
                    outcome?.success(tickets)
                })
                .addTo(compositeDisposable)
    }

    fun create(ticket: Ticket, completionHandler: ((Boolean) -> Unit)? = null) {
        ticketFormOutcome.loading(true)

        val ticketRemoteData = TicketRemoteData()
        ticketRemoteData.create(ticket)
                .performOnBackOutOnMain(scheduler)
                .subscribe({
                    TicketLocalData.update(ticket, it.toTicket())

                    ticketFormOutcome.success(it)
                    completionHandler?.invoke(true)
                }, {
                    ticketFormOutcome.success(TicketCreationResponseData())
                    completionHandler?.invoke(false)
                })
                .addTo(compositeDisposable)
    }
}