package com.pableiros.tickets.model.entities

import io.realm.Realm
import io.realm.RealmObject

open class ModuloRealm: RealmObject() {

    var id: String? = null
    var nombre: String? = null

    fun toModulo(): Modulo {
        return Modulo(id, nombre)
    }

    companion object {
        fun create(modulo: Modulo, realm: Realm): ModuloRealm {
            val moduloRealm = realm.createObject(ModuloRealm::class.java)
            moduloRealm.id = modulo.id
            moduloRealm.nombre = modulo.nombre
            return moduloRealm
        }
    }
}