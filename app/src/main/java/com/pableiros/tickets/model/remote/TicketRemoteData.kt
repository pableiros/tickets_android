package com.pableiros.tickets.model.remote

import com.pableiros.tickets.common.singleton.RxApplication
import com.pableiros.tickets.model.entities.Ticket
import com.pableiros.tickets.model.entities.TicketCreationResponseData
import io.reactivex.Single

class TicketRemoteData {

    fun getTickets(): Single<List<Ticket>> {
        val ticketNetworkService = RxApplication.getNetworkService()!!.retrofitApi
        return ticketNetworkService.getTickets()
    }

    fun create(ticket: Ticket): Single<TicketCreationResponseData> {
        val ticketNetworkService = RxApplication.getNetworkService()!!.retrofitApi
        return ticketNetworkService.createTicket(ticket.toTicketCreationRequestData())
    }
}