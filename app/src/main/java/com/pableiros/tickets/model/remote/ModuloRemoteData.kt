package com.pableiros.tickets.model.remote

import com.pableiros.tickets.common.singleton.RxApplication
import com.pableiros.tickets.model.entities.Modulo
import io.reactivex.Single

class ModuloRemoteData {

    fun getModulos(): Single<List<Modulo>> {
        val ticketNetworkService = RxApplication.getNetworkService()!!.retrofitApi
        return ticketNetworkService.getModulos()
    }
}
