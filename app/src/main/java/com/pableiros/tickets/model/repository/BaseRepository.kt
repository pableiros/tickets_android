package com.pableiros.tickets.model.repository

import android.content.Context
import android.util.LruCache
import com.pableiros.tickets.common.*
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import io.reactivex.functions.Consumer

abstract class BaseRepository<T>() {

    var successHandler: (() -> Unit)? = null
    var errorCompletionHandler: (() -> Unit)? = null

    protected abstract val cacheKey: String
    protected abstract fun buildFreshObservable(): Single<T>

    protected lateinit var responseCache: LruCache<String, Single<*>>

    constructor(responseCache: LruCache<String, Single<*>>): this() {
        this.responseCache = responseCache
    }

    open var outcome: PublishSubject<Outcome<T>>? = null

    protected var scheduler: Scheduler = AppScheduler()
    private lateinit var internetConnection: InternetConnection
    protected var compositeDisposable = CompositeDisposable()
    protected var errorHandler: ErrorHandler? = null

    fun initInternetConnection(context: Context) {
        internetConnection = DeviceInternetConnection(context)
        errorHandler = ErrorHandler(internetConnection)
    }

    fun clearOutcome() {
        outcome?.clear()
    }

    protected open fun performLocalRequest(observable: Single<T>) {
        observable
            .performOnBackOutOnMain(scheduler)
            .subscribe(
                onSuccessCallBack,
                onErrorCallback
            )
            .addTo(compositeDisposable)
    }

    protected fun getCachedObservable(): Single<T> {
        @Suppress("UNCHECKED_CAST")
        return responseCache[cacheKey] as Single<T>? ?: buildFreshObservable()
    }

    protected open val onSuccessCallBack = Consumer<T> {
        outcome?.success(it)
    }

    protected open val onErrorCallback = Consumer<Throwable> {
        errorHandler?.handle(it)?.let {networkError ->
            outcome?.failed(networkError)
        }
    }
}