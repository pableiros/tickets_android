package com.pableiros.tickets.model.local

import com.pableiros.tickets.model.entities.Ticket
import com.pableiros.tickets.model.entities.TicketRealm
import io.reactivex.Observable
import io.reactivex.Single
import io.realm.Realm
import java.text.SimpleDateFormat
import java.util.*

object TicketLocalData {

    fun getAll(): List<Ticket> {
        val realm = Realm.getDefaultInstance()
        val ticketsRealm = realm.where(TicketRealm::class.java).findAll()
        val tickets = ticketsRealm.map { it.toTicket() }

        realm.close()

        return tickets
    }

    fun store(tickets: List<Ticket>) {
        val realm = Realm.getDefaultInstance()

        realm.executeTransaction { bgRealm ->
            val existingTickets = realm.where(TicketRealm::class.java).isNotNull("folio").findAll()
            existingTickets.deleteAllFromRealm()

            val ticketsRealm = tickets.asSequence().map { TicketRealm.create(it, bgRealm) }.toList()
            bgRealm.insert(ticketsRealm)
        }

        realm.close()
    }

    fun update(ticket: Ticket, newTicket: Ticket) {
        val realm = Realm.getDefaultInstance()

        realm.where(TicketRealm::class.java)
                .equalTo("syncId", ticket.syncId)
                .findFirst()
                ?.let { ticketRealm ->
                    realm.executeTransaction {
                        ticketRealm.folio = newTicket.folio
                        ticketRealm.id = newTicket.id
                    }
                }

        realm.close()
    }

    fun create(ticket: Ticket) {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        ticket.fechaInicio = simpleDateFormat.format(Date())
        ticket.syncId = UUID.randomUUID().toString()

        val realm = Realm.getDefaultInstance()

        realm.executeTransaction {
            val ticketRealm = TicketRealm.create(ticket, it)
            it.insert(ticketRealm)
        }

        realm.close()
    }

    fun getPendingToSync(): List<Ticket> {
        val realm = Realm.getDefaultInstance()
        val ticketsRealm = realm.where(TicketRealm::class.java).isNull("folio").findAll()
        val tickets = ticketsRealm.map { it.toTicket() }

        realm.close()

        return tickets
    }

    fun getTicket(ticket: Ticket): Ticket? {
        val realm = Realm.getDefaultInstance()
        val ticketRealm = realm.where(TicketRealm::class.java)
                .equalTo("syncId", ticket.syncId)
                .findAll()
                .firstOrNull()

        val ticketToReturn = if (ticketRealm != null) {
            ticketRealm.toTicket()
        } else {
            ticket
        }

        realm.close()
        return ticketToReturn

    }
}