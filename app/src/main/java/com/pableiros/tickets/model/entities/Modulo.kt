package com.pableiros.tickets.model.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.pableiros.tickets.R
import com.pableiros.tickets.common.singleton.RxApplication

class Modulo(
        @SerializedName("id")
        var id: String?,
        @SerializedName("nombre")
        var nombre: String?
): Parcelable {

    private constructor(p: Parcel?): this(
            id = p?.readString(),
            nombre = p?.readString()
    )

    override fun toString(): String {
        return nombre ?: ""
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        p0?.writeString(id)
        p0?.writeString(nombre)
    }

    override fun describeContents() = 0

    companion object {
        @JvmField val CREATOR = object : Parcelable.Creator<Modulo> {
            override fun createFromParcel(p0: Parcel?) = Modulo(p0)

            override fun newArray(p0: Int) = arrayOfNulls<Modulo>(p0)
        }

        fun createComprasInstance(): Modulo {
            return Modulo(
                    "1",
                    RxApplication.getContext()?.getString(R.string.compras)
            )
        }

        fun createGastosViajeInstance(): Modulo {
            return Modulo(
                    "2",
                    RxApplication.getContext()?.getString(R.string.gastos_viaje)
            )
        }

        fun createMantenimientoInstance(): Modulo {
            return Modulo(
                    "3",
                    RxApplication.getContext()?.getString(R.string.mantenimiento)
            )
        }

        fun createProcesosInstance(): Modulo {
            return Modulo(
                    "4",
                    RxApplication.getContext()?.getString(R.string.procesos)
            )
        }

        fun createRecursosHumanosInstance(): Modulo {
            return Modulo(
                    "5",
                    RxApplication.getContext()?.getString(R.string.recursos_humanos)
            )
        }
    }
}