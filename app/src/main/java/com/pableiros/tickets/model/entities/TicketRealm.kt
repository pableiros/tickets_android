package com.pableiros.tickets.model.entities

import io.realm.Realm
import io.realm.RealmObject

open class TicketRealm: RealmObject() {

    var id: String? = null
    var fechaInicio: String? = null
    var fechaFin: String? = null
    var folio: String? = null
    var descripcion: String? = null
    var estatus: String? = null
    var moduloId: String? = null
    var moduloNombre: String? = null
    var syncId: String? = null

    fun toTicket(): Ticket {
        return Ticket(
            id = id,
            fechaInicio =  fechaInicio,
            fechaFin = fechaFin,
            folio = folio,
            descripcion = descripcion,
            estatus = estatus,
            moduloId = moduloId,
            moduloNombre = moduloNombre,
            syncId = syncId
        )
    }

    companion object {
        fun create(ticket: Ticket, realm: Realm): TicketRealm {
            val ticketRealm = realm.createObject(TicketRealm::class.java)
            ticketRealm.id = ticket.id
            ticketRealm.fechaInicio = ticket.fechaInicio
            ticketRealm.fechaFin = ticket.fechaFin
            ticketRealm.folio = ticket.folio
            ticketRealm.descripcion = ticket.descripcion
            ticketRealm.estatus = ticket.estatus
            ticketRealm.moduloId = ticket.moduloId
            ticketRealm.moduloNombre = ticket.moduloNombre
            ticketRealm.syncId = ticket.syncId
            return ticketRealm
        }
    }
}