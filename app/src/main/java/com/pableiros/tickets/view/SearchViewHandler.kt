package com.pableiros.tickets.view

import android.arch.lifecycle.*
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.SearchView
import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

class SearchViewHandler(private val lifecycleOwner: LifecycleOwner,
                        private val searchListener: SearchListener,
                        private val swipeRefreshLayout: SwipeRefreshLayout?,
                        private val liveDataSearchQuery: MutableLiveData<String>
): LifecycleObserver {

    private val searchDisposable = CompositeDisposable()

    var searchView: SearchView? = null

    fun initialize(searchView: SearchView) {
        this.searchView = searchView

        lifecycleOwner.lifecycle.addObserver(this)

        setupObservables()
        observeSearchQuery()
        restoreSearchViewState()
    }

    private fun setupObservables() {
        if (searchView != null) {
            val searchObservable = RxSearchView.queryTextChanges(searchView!!).skipInitialValue().share()

            searchObservable.skip(1).debounce(0, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    liveDataSearchQuery.postValue(it.toString())
                    swipeRefreshLayout?.isRefreshing = false

                }.addTo(searchDisposable)

            searchObservable.skip(1).observeOn(AndroidSchedulers.mainThread())
                .subscribe { swipeRefreshLayout?.isRefreshing = false }
                .addTo(searchDisposable)
        }
    }

    private fun observeSearchQuery() {
        liveDataSearchQuery.observe(lifecycleOwner, Observer { searchQuery ->
            searchQuery?.let { onSearchQuerySubmit(it) } ?: onSearchQueryCleared()
        })
    }

    private fun restoreSearchViewState() {
        val searchQuery = liveDataSearchQuery.value
        if (searchQuery.isNullOrBlank()) {
            resetSearchView()
        } else {
            setQueryOnSearchView(searchQuery)
        }
    }

    private fun setQueryOnSearchView(searchQuery: String?) {
        searchView?.isIconified = false
        searchView?.setQuery(searchQuery, true)
        searchView?.clearFocus()
    }

    fun resetSearchView() {
        searchView?.onActionViewCollapsed()
        searchView?.setQuery("", true)
        searchView?.clearFocus()
    }

    private fun onSearchQuerySubmit(searchQuery: String) {
        searchListener.onSearchQuerySubmit(searchQuery)
    }

    private fun onSearchQueryCleared() {
        resetSearchView()
        searchListener.onSearchQueryCleared()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun unsubscribe() {
        searchDisposable.dispose()
    }

    interface SearchListener {
        fun onSearchQuerySubmit(searchQuery: String)
        fun onSearchQueryCleared()
    }

    private fun Disposable.addTo(compositeDisposable: CompositeDisposable) {
        compositeDisposable.add(this)
    }
}