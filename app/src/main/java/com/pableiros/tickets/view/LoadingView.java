package com.pableiros.tickets.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.*;
import com.pableiros.tickets.R;

public class LoadingView extends FrameLayout {

    protected LoadingViewInteractionListener loadingViewInteractionListener;

    protected ProgressBar progressBar;

    public LoadingView(@NonNull Context context) {
        this(context, null);
    }

    public LoadingView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LoadingView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(getContext(), R.layout.loading_view, this);
        progressBar = findViewById(R.id.progress_bar);

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LoadingView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflate(getContext(), R.layout.loading_view, this);
        progressBar = findViewById(R.id.progress_bar);
    }

    public void setInteractionListener(LoadingViewInteractionListener loadingViewInteractionListener) {
        this.loadingViewInteractionListener = loadingViewInteractionListener;
    }

    public void showLoading() {
        progressBar.setVisibility(VISIBLE);

        if (loadingViewInteractionListener != null) {
            loadingViewInteractionListener.hideContentLayout();
        }

        this.setVisibility(VISIBLE);
    }

    public void hideLoadingLayout() {
        this.setVisibility(GONE);

        if (loadingViewInteractionListener != null) {
            loadingViewInteractionListener.showContentLayout();
        }
    }

    public interface LoadingViewInteractionListener {
        void showContentLayout();
        void hideContentLayout();
    }
}
