package com.pableiros.tickets.view

import android.support.v4.app.FragmentTransaction
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.pableiros.tickets.common.Keyboard


abstract class BaseAppCompatActivity: AppCompatActivity() {

    protected abstract val contentId: Int

    fun openFragmentWithBackStack(fragment: Fragment, fragmentTag: String) {
        Keyboard.hideKeyboard(this)
        supportFragmentManager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(contentId, fragment, fragmentTag)
            .addToBackStack(fragmentTag)
            .commit()
    }

    fun openFragment(fragment: Fragment, fragmentTag: String) {
        Keyboard.hideKeyboard(this)
        supportFragmentManager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(contentId, fragment, fragmentTag)
            .commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    @SuppressWarnings("unused")
    protected fun showToastLong(message: String) {
        val baseFragmentHelper = BaseFragmentHelper(this)
        baseFragmentHelper.showToastLong(message, this)
    }

}