package com.pableiros.tickets.view

import android.app.Activity
import android.content.Context
import android.widget.Toast
import com.pableiros.tickets.common.NetworkError
import com.pableiros.tickets.common.Outcome
import com.pableiros.tickets.R

class BaseFragmentHelper(val context: Context) {

    fun handleFailureActionOutcome(outcome: Outcome.Failure<Any>, errorConnectionHandler: () -> Unit, failureOutcomeHandler: (() -> Unit)? = null) {
        var errorMessage: String? = null
        var isFailureOutcomeHandlerAvailable = false

        when(outcome.e) {
            is NetworkError.RequestError -> {
                errorMessage = cleanRequestError(outcome.e.errorMessage)
                isFailureOutcomeHandlerAvailable = true
            }
            is NetworkError.ConnectionError -> errorConnectionHandler()
            is NetworkError.ServerError -> {
                errorMessage = context.getString(R.string.message_server_error)
                isFailureOutcomeHandlerAvailable = true
            }
        }

        if (errorMessage != null) {
            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
        }

        if (isFailureOutcomeHandlerAvailable) {
            failureOutcomeHandler?.invoke()
        }
    }

    private fun cleanRequestError(errorMessage: String): String {
        val errors = errorMessage.split("|")
        return if (errors.size > 1) errors[1].trim() else errorMessage
    }

    fun showToastLong(message: String, activity: Activity?) {
        activity?.runOnUiThread {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        }
    }
}