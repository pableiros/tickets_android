package com.pableiros.tickets.view

import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.pableiros.tickets.common.NetworkError
import com.pableiros.tickets.common.Outcome
import com.pableiros.tickets.R


open class BaseListFragment: BaseFragment() {

    protected lateinit var searchViewHandler: SearchViewHandler
    lateinit var actionModeCallback: ActionModeCallback
    protected open lateinit var actionModeView: ActionModeCallback.ActionModeView
    protected var loadingView: LoadingView? = null

    protected lateinit var emptyListMessage: String
    protected var swipeRefreshLayout: SwipeRefreshLayout? = null

    protected fun handleFailureListOutcome(recyclerView: RecyclerView, outcome: Outcome.Failure<Any>) {
        var errorMessage: String? = null

        when (outcome.e) {
            is NetworkError.RequestError -> errorMessage = outcome.e.errorMessage
            is NetworkError.ConnectionError -> errorMessage = getString(R.string.message_internet_connection)
            is NetworkError.ServerError -> errorMessage = getString(R.string.message_server_error)
        }
    }

    protected fun prepareRecyclerViewForEmptyList(recyclerView: RecyclerView) {
        recyclerView.isNestedScrollingEnabled = false
        recyclerView.layoutManager = LinearLayoutManager(context)

        val itemDecoration: RecyclerView.ItemDecoration? = null

        while (recyclerView.itemDecorationCount > 0) {
            itemDecoration?.let {
                recyclerView.removeItemDecoration(it)
            }
        }

        recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {})
    }

    protected fun <T> validateOutcomeFromResult(
        outcome: Outcome<List<T>>?,
        recyclerView: RecyclerView
    ) {
        swipeRefreshLayout?.isEnabled = false

        when (outcome) {
            is Outcome.Progress -> {
                if (outcome.isFirstLoad) {
                    loadingView?.showLoading()
                    showLoadingViews()
                } else {
                    swipeRefreshLayout?.isRefreshing = true
                }
            }

            is Outcome.Success -> {
                swipeRefreshLayout?.isEnabled = true
                hideLoadingViews()

                if (outcome.data.isEmpty()) {
                    prepareRecyclerViewForEmptyList(recyclerView)
                } else {
                    showListFromResult(outcome)
                }
            }

            is Outcome.Failure -> {
                swipeRefreshLayout?.isEnabled = true
                hideLoadingViews()

                @Suppress("UNCHECKED_CAST")
                handleFailureListOutcome(recyclerView, outcome as Outcome.Failure<Any>)
            }
        }
    }

    protected open fun showLoadingViews() {
    }

    protected open fun hideLoadingViews() {
        loadingView?.hideLoadingLayout()
        swipeRefreshLayout?.isRefreshing = false
        swipeRefreshLayout?.isEnabled = true
    }

    open fun <T> showListFromResult(outcome: Outcome<List<T>>?) {
        throw Exception("not implemented")
    }

    protected fun resetSearchView() {
        if (searchViewHandler.searchView?.isIconified == false) {
            searchViewHandler.resetSearchView()
        }
    }

}