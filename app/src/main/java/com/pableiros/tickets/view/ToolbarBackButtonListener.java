package com.pableiros.tickets.view;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class ToolbarBackButtonListener implements View.OnClickListener {

    private AppCompatActivity activity;

    public ToolbarBackButtonListener(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onClick(View v) {
        if (isFragmentInBackStack()) {
            activity.getSupportFragmentManager().popBackStack();
        } else {
            activity.finish();
        }
    }

    private boolean isFragmentInBackStack() {
        return activity.getSupportFragmentManager().getBackStackEntryCount() != 0;
    }
}
