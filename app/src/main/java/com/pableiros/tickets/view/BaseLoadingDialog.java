package com.pableiros.tickets.view;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.pableiros.tickets.R;

import org.jetbrains.annotations.NotNull;

public abstract class BaseLoadingDialog extends DialogFragment {

    private boolean loading;
    private String LOADING_KEY = "LOADING";

    public static String getTagString() {
        return "BASE_LOADING_DIALOG";
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog progressDialog = new ProgressDialog(getContext());

        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);

        progressDialog.setTitle(getTitle());
        progressDialog.setMessage(getMessage());

        if (savedInstanceState != null) {
            loading = savedInstanceState.getBoolean(LOADING_KEY);
        }

        setCancelable(false);

        return progressDialog;
    }

    @NonNull
    protected String getTitle() {
        return getString(R.string.dialog_loading_base_title);
    }

    @NonNull
    protected String getMessage() {
        return getString(R.string.dialog_loading_base_message);
    }

    @Override
    public void onSaveInstanceState(@NotNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(LOADING_KEY, loading);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (loading) {
            consume(true);
        } else {
            loading = true;
            consume(false);
        }
    }

    public abstract void consume(boolean useCache);

}
