package com.pableiros.tickets.view

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.ViewGroup
import com.pableiros.tickets.R
import com.pableiros.tickets.databinding.ListItemSingleCheckableBinding
import java.util.ArrayList

class SelectableItemRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private var listView: View? = null

    private var objectList: MutableList<Any>? = null
    private var auxObjectList: MutableList<Any>? = null

    var lastCheckedItem: Any? = null
    private var longClickPosition = -1

    val context: Context?
        get() = listView?.getContext()

    @Suppress("unused")
    val longClickSelectedItem: Any
        get() {
            val item = objectList!![longClickPosition]
            longClickPosition = -1
            return item
        }

    constructor(listView: View, objectList: List<Any>) {
        this.listView = listView

        this.objectList = ArrayList()
        this.objectList?.addAll(objectList)

        this.auxObjectList = ArrayList()
        this.auxObjectList?.addAll(objectList)
    }

    constructor(listView: View) {
        this.listView = listView
        this.objectList = ArrayList()
        this.auxObjectList = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ListItemSingleCheckableBinding>(
                LayoutInflater.from(context),
                R.layout.list_item_single_checkable,
                parent,
                false)
        return SelectableItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val item = objectList!![position]
        val holder = viewHolder as SelectableItemViewHolder

        var isSelected = false

        if (lastCheckedItem != null) {
            isSelected = item.toString() == lastCheckedItem?.toString()
        }

        holder.binding?.name = item.toString()
        holder.binding?.selected = isSelected

        val selected = holder.adapterPosition == longClickPosition
        holder.setSelected(selected)

        holder.binding?.onClickListener = android.view.View.OnClickListener { listView?.onItemSelected(item) }

        holder.binding?.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return objectList!!.size
    }

    fun swapData(updatedObjectList: List<Any>?) {
        objectList?.clear()
        auxObjectList?.clear()

        updatedObjectList?.let {
            objectList?.addAll(it)
            auxObjectList?.addAll(it)
        }

        notifyDataSetChanged()
    }

    fun filter(text: String) {
        var textMutable = text

        if (textMutable.isEmpty()) {
            objectList?.clear()
            objectList?.addAll(auxObjectList!!)
        } else {
            val result = ArrayList<Any>()
            textMutable = textMutable.toLowerCase()

            for (item in auxObjectList!!) {
                if (item.toString().toLowerCase().contains(textMutable))
                    result.add(item)
            }

            objectList?.clear()
            objectList?.addAll(result)
        }
        notifyDataSetChanged()
    }

    fun clearList() {
        objectList?.clear()
        auxObjectList?.clear()
        notifyDataSetChanged()
    }

    @Suppress("unused")
    fun setLongClickPosition(longClickPosition: Int) {
        this.longClickPosition = longClickPosition
        notifyDataSetChanged()
    }

    fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(LONG_CLICK_POSITION_KEY, longClickPosition)
    }

    @Suppress("unused")
    fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            longClickPosition = savedInstanceState.getInt(LONG_CLICK_POSITION_KEY)
        }
    }

    internal inner class SelectableItemViewHolder(itemView: android.view.View) : RecyclerView.ViewHolder(itemView) {
        var binding: ListItemSingleCheckableBinding? = null

        private var isSelected: Boolean = false

        init {
            binding = DataBindingUtil.bind(itemView)
        }

        fun setSelected(selected: Boolean) {

            if (selected) {
                if (!isSelected) {
                    context?.let {
                        binding?.layout?.setBackgroundColor(ContextCompat.getColor(it, R.color.grey_400))
                    }
                }
            } else {
                if (isSelected) {
                    val outValue = TypedValue()
                    context?.theme?.resolveAttribute(android.R.attr.selectableItemBackground, outValue, true)
                    binding?.layout?.setBackgroundResource(outValue.resourceId)
                }
            }

            isSelected = selected
        }
    }

    interface View {
        fun getContext(): Context?

        fun onItemSelected(selectedItem: Any?)
    }

    companion object {

        private const val LONG_CLICK_POSITION_KEY = "LONG_CLICK_POSITION"
    }
}
