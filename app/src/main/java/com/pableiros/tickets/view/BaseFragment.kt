package com.pableiros.tickets.view

import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.Toast
import com.pableiros.tickets.common.Keyboard
import com.pableiros.tickets.common.Outcome
import com.pableiros.tickets.R

open class BaseFragment: Fragment() {

    @SuppressWarnings("WeakerAccess")
    var okButton: Button? = null

    override fun onStop() {
        Keyboard.hideKeyboard(activity)
        super.onStop()
    }

    protected fun getFragmentByTag(tag: String): Fragment? {
        return activity?.supportFragmentManager?.findFragmentByTag(tag)
    }

    protected fun setToolbarTitle(title: String) {
        val activity = activity

        if (activity != null) {
            val appCompatActivity = activity as AppCompatActivity?

            if (appCompatActivity?.supportActionBar != null) {
                appCompatActivity.supportActionBar?.title = title
            }
        }
    }

    protected fun popBackStack() {
        if (activity != null) {
            Keyboard.hideKeyboard(activity!!)
            activity!!.supportFragmentManager.popBackStack()
        }
    }

    protected fun showSnackbarLong(resString: Int) {
        if (activity != null) {
            activity?.runOnUiThread {
                Snackbar.make(activity!!.findViewById(android.R.id.content), resString, Snackbar.LENGTH_LONG).show()
            }
        }
    }

    protected fun showSnackbarLong(text: CharSequence) {
        if (activity != null) {
            activity?.runOnUiThread {
                Snackbar.make(activity!!.findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG).show()
            }
        }
    }

    protected fun showToastLong(message: String) {
        val baseFragmentHelper = BaseFragmentHelper(context!!)
        baseFragmentHelper.showToastLong(message, activity)
    }

    protected fun openFragmentWithBackStack(fragment: Fragment, fragmentTag: String) {
        val baseAppCompatActivity = activity as BaseAppCompatActivity?
        baseAppCompatActivity?.openFragmentWithBackStack(fragment, fragmentTag)
    }

    protected fun openFragment(fragment: Fragment, fragmentTag: String) {
        val baseAppCompatActivity = activity as BaseAppCompatActivity?
        baseAppCompatActivity?.openFragment(fragment, fragmentTag)
    }

    protected fun handleFailureActionOutcome(outcome: Outcome.Failure<Any>) {
        val baseFragmentHelper = BaseFragmentHelper(context!!)
        baseFragmentHelper.handleFailureActionOutcome(
            outcome,
            { handleErrorConnection() },
            { failureOutcomeHandler() })
    }

    protected open fun failureOutcomeHandler() {
        activity?.runOnUiThread {
            okButton?.isEnabled = true
        }
    }

    protected open fun handleErrorConnection() {
        val errorMessage = getString(R.string.message_internet_connection)
        Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
    }
}
