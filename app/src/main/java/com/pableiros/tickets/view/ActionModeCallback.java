package com.pableiros.tickets.view;


import android.app.Activity;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class ActionModeCallback implements ActionMode.Callback {

    private ActionModeView actionModeView;
    private Activity activity;
    private int menuResId;

    private static final String TITLE_KEY = "TITLE";
    private String title;
    private static final String SHOWING_KEY = "SHOWING";
    private boolean isShowing;

    private ActionMode actionMode;

    private boolean isHideOnClick = true;

    public ActionModeCallback(ActionModeView actionModeView, Activity activity, int menuResId) {
        this.actionModeView = actionModeView;
        this.activity = activity;
        this.menuResId = menuResId;
    }

    public void showActionMode(String actionTitle) {
        title = actionTitle;
        isShowing = true;

        if (actionMode != null) {
            actionMode.setTitle(title);
        } else {
            activity.startActionMode(this);
        }
        updateActionMode(actionMode);
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(menuResId, menu);
        actionMode = mode;
        actionMode.setTitle(title);
        actionModeView.onActionModeCreated();
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        boolean bool = actionModeView.onActionItemClicked(item.getItemId());
        if (isHideOnClick) {
            hideActionMode();
        }

        return bool;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        actionMode = null;
        isShowing = false;
        actionModeView.onActionModeDestroyed();
    }

    public void hideActionMode() {
        if (actionMode != null) {
            actionMode.finish();
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(SHOWING_KEY, isShowing);
        outState.putString(TITLE_KEY, title);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            isShowing = savedInstanceState.getBoolean(SHOWING_KEY);
            title = savedInstanceState.getString(TITLE_KEY);
            if (isShowing) {
                showActionMode(title);
            }
        }
    }

    public void setHideOnClick(boolean hideOnClick) {
        isHideOnClick = hideOnClick;
    }


    public void updateActionMode(ActionMode actionMode) {

    }

    public interface ActionModeView {
        void onActionModeCreated();
        boolean onActionItemClicked(int itemClicked);
        void onActionModeDestroyed();
    }

}

