package com.pableiros.tickets.presentation

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class BaseListViewModel: ViewModel() {

    val searchQuery: MutableLiveData<String> = MutableLiveData()
    val selectedIndex: MutableLiveData<Int> = MutableLiveData()
    val compositeDisposable: CompositeDisposable = CompositeDisposable()

    init {
        searchQuery.value = null
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}