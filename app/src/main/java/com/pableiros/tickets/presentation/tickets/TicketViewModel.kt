package com.pableiros.tickets.presentation.tickets

import android.arch.lifecycle.LiveData
import com.pableiros.tickets.common.Outcome
import com.pableiros.tickets.common.toLiveData
import com.pableiros.tickets.model.entities.Ticket
import com.pableiros.tickets.model.entities.TicketCreationResponseData
import com.pableiros.tickets.model.local.TicketLocalData
import com.pableiros.tickets.model.repository.TicketRepository
import com.pableiros.tickets.presentation.BaseListViewModel

class TicketViewModel: BaseListViewModel() {

    val ticketOutcome: LiveData<Outcome<List<Ticket>>> by lazy {
        ticketRepository.outcome!!.toLiveData(compositeDisposable)
    }

    val ticketFormOutcome: LiveData<Outcome<TicketCreationResponseData>> by lazy {
        ticketRepository.ticketFormOutcome.toLiveData(compositeDisposable)
    }

    private val ticketRepository = TicketRepository()

    fun fetch() {
        ticketRepository.fetch()
    }

    fun refresh(showLoading: Boolean) {
        ticketRepository.refresh(showLoading)
    }

    fun create(ticket: Ticket) {
        TicketLocalData.create(ticket)
        ticketRepository.create(ticket)
    }
}