package com.pableiros.tickets.presentation.ticketform.modulos

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

class ModuloViewModelFactory: ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return ModuloViewModel() as T
    }
}