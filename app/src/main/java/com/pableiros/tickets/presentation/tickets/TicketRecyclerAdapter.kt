package com.pableiros.tickets.presentation.tickets

import android.annotation.SuppressLint
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pableiros.tickets.R
import com.pableiros.tickets.common.DateTools
import com.pableiros.tickets.model.entities.Ticket
import kotlinx.android.synthetic.main.list_item_ticket.view.*

class TicketRecyclerAdapter(
    val interactionListener: TicketRecyclerAdapterInteractionListener
): ListAdapter<Ticket, TicketRecyclerAdapter.TicketViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TicketViewHolder {
         val view = LayoutInflater.from(p0.context).inflate(R.layout.list_item_ticket, p0, false)
        return TicketViewHolder(view)
    }

    override fun onBindViewHolder(p0: TicketViewHolder, p1: Int) {
        p0.bindTo(getItem(p1))
    }

    fun submitList(ticketsList: List<Ticket>, search: String?) {
        if (ticketsList.isEmpty() || search.isNullOrEmpty()) {
            submitList(ticketsList)
        } else {
            val filteredList = ticketsList.filter {
                it.descripcion!!.contains(search, true)
                        || DateTools.apiDateToFormattedDate(it .fechaInicio).contains(search, true)
                        || it.moduloNombre?.contains(search, true) == true
            }

            submitList(filteredList)
        }
    }

    inner class TicketViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindTo(ticket: Ticket) {
            if (ticket.descripcion?.length ?: 0 > 70) {
                itemView.textView_descripcion.text =  "${ticket.descripcion?.substring(0, Math.min(ticket.descripcion!!.length, 70))}..."
            } else {
                itemView.textView_descripcion.text = ticket.descripcion
            }

            itemView.textView_modulo.text = ticket.moduloNombre
            itemView.textView_fecha.text = DateTools.apiDateToFormattedDate(ticket.fechaInicio)

            itemView.linearLayout_ticket.setOnClickListener {
                interactionListener.onClick(ticket)
            }
        }
    }

    interface TicketRecyclerAdapterInteractionListener {
        fun onClick(ticket: Ticket)
    }

    companion object {
        val DIFF_CALLBACK = object: DiffUtil.ItemCallback<Ticket>() {
            override fun areItemsTheSame(p0: Ticket, p1: Ticket): Boolean {
                return p0.id == p1.id
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(p0: Ticket, p1: Ticket): Boolean {
                return p0 == p1
            }
        }
    }
}