package com.pableiros.tickets.presentation.ticketform

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.view.*
import com.pableiros.tickets.R
import com.pableiros.tickets.common.Keyboard
import com.pableiros.tickets.common.Outcome
import com.pableiros.tickets.common.reObserve
import com.pableiros.tickets.model.entities.Modulo
import com.pableiros.tickets.model.entities.Ticket
import com.pableiros.tickets.model.entities.TicketCreationResponseData
import com.pableiros.tickets.presentation.ticketform.modulos.ModulosFragment
import com.pableiros.tickets.presentation.tickets.TicketViewModel
import com.pableiros.tickets.presentation.tickets.TicketViewModelFactory
import com.pableiros.tickets.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_ticket_form.*
import java.lang.RuntimeException

class TicketFormFragment: BaseFragment() {

    private var moduloSelected: Modulo? = null
    private var descripcion: String? = null
    private var moduloLiveData: MutableLiveData<Modulo> = MutableLiveData()
    private var ticketFormDialog: TicketFormDialog? = null
    private var ticketFormOnFragmentInteractionListener: TicketFormOnFragmentInteractionListener? = null

    private val ticketViewModel: TicketViewModel by lazy {
        val ticketViewModelFactory = TicketViewModelFactory()
        ViewModelProviders.of(this, ticketViewModelFactory).get(TicketViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        moduloSelected = savedInstanceState?.getParcelable(MODULO_SELECTED_ARG)
        descripcion = savedInstanceState?.getString(DESCRIPCION_ARG)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)

        return inflater.inflate(R.layout.fragment_ticket_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        linearLayout_modulo.setOnClickListener {
            openFragmentWithBackStack(ModulosFragment.createNewInstance(moduloSelected), ModulosFragment.TAG)
        }

        moduloLiveData.reObserve(this, moduloObserver)

        moduloSelected?.let {
            moduloLiveData.value = it
        }

        editText_descripcion.setText(descripcion)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putParcelable(MODULO_SELECTED_ARG, moduloSelected)
        outState.putString(DESCRIPCION_ARG, editText_descripcion.text.toString())
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.menu_form, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId) {
            R.id.ok_option -> {
                performSaveTicket()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is TicketFormOnFragmentInteractionListener) {
            ticketFormOnFragmentInteractionListener = context
        } else {
            throw RuntimeException(context.toString() + " is not implemented TicketFormOnFragmentInteractionListener")
        }
    }


    fun setModulo(modulo: Modulo?) {
        moduloLiveData.value = modulo
    }

    private var moduloObserver = Observer<Modulo> {
        textView_modulo_value.text = it?.nombre
        moduloSelected = it
    }

    private fun performSaveTicket() {
        if (isFormValid()) {
            Keyboard.hideKeyboard(activity!!)
            ticketFormDialog = TicketFormDialog()
            ticketFormDialog?.show(requireFragmentManager(),TicketFormDialog.TAG)
            ticketViewModel.ticketFormOutcome.reObserve(this, ticketCreationObserver)

            val ticket = Ticket(
                    null,
                    null,
                    null,
                    null,
                    editText_descripcion.text.toString(),
                    moduloId = moduloSelected?.id,
                    moduloNombre = moduloSelected?.nombre,
                    syncId = null,
                    estatus = "1"
            )

            ticketViewModel.create(ticket)
        }
    }

    private fun isFormValid(): Boolean {
        var message: String? = null

        if (moduloSelected == null) {
            message = getString(R.string.no_module_selected_message)
        }

        if (editText_descripcion.text.isNullOrEmpty() && message == null) {
            message = getString(R.string.no_description_message)
        }

        val isFormValid = message == null

        if (!isFormValid) {
            showSnackbarLong(message!!)
        }

        return isFormValid
    }

    private val ticketCreationObserver = Observer<Outcome<TicketCreationResponseData>> { outcome ->
        when(outcome) {
            is Outcome.Success -> {
                ticketFormOnFragmentInteractionListener?.handleTicketCreated()
            }
        }
    }

    interface TicketFormOnFragmentInteractionListener {
        fun handleTicketCreated()
    }


    companion object {
        const val TAG = "TicketFormFragment"

        private const val MODULO_SELECTED_ARG = "MODULO_SELECTED_ARG"
        private const val DESCRIPCION_ARG = "DESCRIPCION_ARG"
    }
}