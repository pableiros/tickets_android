package com.pableiros.tickets.presentation.ticketdetalle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pableiros.tickets.R
import com.pableiros.tickets.common.DateTools
import com.pableiros.tickets.model.entities.Ticket
import com.pableiros.tickets.model.local.TicketLocalData
import com.pableiros.tickets.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_ticket_detalle.*

class TicketDetalleFragment: BaseFragment(){

    private var ticket: Ticket? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ticket = if (savedInstanceState != null) {
            savedInstanceState.getParcelable(TICKET_ARG)
        } else {
            arguments?.getParcelable(TICKET_ARG)
        }

        if (ticket != null) {
            ticket = TicketLocalData.getTicket(ticket!!)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setToolbarTitle(getString(R.string.detail))
        setHasOptionsMenu(false)

        return inflater.inflate(R.layout.fragment_ticket_detalle, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        textView_date.text = DateTools.apiDateToFormattedDate(ticket?.fechaInicio)
        textView_folio.text = if (!ticket?.folio.isNullOrEmpty()) {
            textview_footer.visibility = View.GONE
            ticket?.folio
        } else {
            textview_footer.visibility = View.VISIBLE
            "--"
        }

        textView_estatus.text = if (ticket?.estatus == "1") {
            getString(R.string.in_process)
        } else {
            "--"
        }

        textView_modulo.text = ticket?.moduloNombre
        textView_descripcion.text = ticket?.descripcion
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putParcelable(TICKET_ARG, ticket)
    }

    companion object {

        const val TAG = "TicketDetalleFragment"

        private const val TICKET_ARG = "TICKET_ARG"

        fun createNewInstance(ticket: Ticket): TicketDetalleFragment {
            val ticketDetalleFragment = TicketDetalleFragment()
            val args = Bundle()
            args.putParcelable(TICKET_ARG, ticket)

            ticketDetalleFragment.arguments = args
            return ticketDetalleFragment
        }
    }
}