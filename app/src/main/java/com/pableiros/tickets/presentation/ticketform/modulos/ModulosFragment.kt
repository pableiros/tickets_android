package com.pableiros.tickets.presentation.ticketform.modulos

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import com.pableiros.tickets.R
import com.pableiros.tickets.common.Outcome
import com.pableiros.tickets.common.reObserve
import com.pableiros.tickets.model.entities.Modulo
import com.pableiros.tickets.presentation.ticketform.TicketFormFragment
import com.pableiros.tickets.presentation.tickets.TicketsFragment
import com.pableiros.tickets.view.*
import kotlinx.android.synthetic.main.fragment_modulos.recycler_view

class ModulosFragment: BaseListFragment(),
        LoadingView.LoadingViewInteractionListener,
        SelectableItemRecyclerAdapter.View,
        SearchViewHandler.SearchListener {

    private val moduloViewModel: ModuloViewModel by lazy {
        val moduloViewModelFactory = ModuloViewModelFactory()
        ViewModelProviders.of(this, moduloViewModelFactory).get(ModuloViewModel::class.java)
    }

    private var selectableItemRecyclerAdapter: SelectableItemRecyclerAdapter? = null
    private var moduloSelected: Modulo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState != null) {
            moduloSelected = savedInstanceState.getParcelable(MODULO_SELECTED_ARG)
        } else {
            moduloSelected = arguments?.getParcelable(MODULO_SELECTED_ARG)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        setToolbarTitle(getString(R.string.modules))

        val view = inflater.inflate(R.layout.fragment_modulos, container, false)

        loadingView = view.findViewById(R.id.loading_view)
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout)

        selectableItemRecyclerAdapter = SelectableItemRecyclerAdapter(this, ArrayList<Modulo>())
        selectableItemRecyclerAdapter?.lastCheckedItem = moduloSelected

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        searchViewHandler = SearchViewHandler(this, this, swipeRefreshLayout, moduloViewModel.searchQuery)
        loadingView?.setInteractionListener(this)

        recycler_view.adapter = selectableItemRecyclerAdapter
        recycler_view.layoutManager = LinearLayoutManager(context)
        recycler_view.addItemDecoration(SimpleDividerItemDecoration(context!!))

        moduloViewModel.modulosOutcome.reObserve(this, modulosListObserver)
        moduloViewModel.fetch()
    }

    private val modulosListObserver = Observer<Outcome<List<Modulo>>> { outcome ->
        validateOutcomeFromResult(outcome, recycler_view)
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T> showListFromResult(outcome: Outcome<List<T>>?) {
        if (outcome == null || outcome !is Outcome.Success) {
            return
        }

        val searchQuery = moduloViewModel.searchQuery.value
        val modulos = outcome.data as List<Modulo>

        if (modulos.isEmpty() || searchQuery.isNullOrEmpty()) {
            selectableItemRecyclerAdapter?.swapData(modulos)
        } else {
            val filteredList = modulos.filter {
                it.nombre!!.contains(searchQuery, true)
            }

            selectableItemRecyclerAdapter?.swapData(filteredList)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putParcelable(MODULO_SELECTED_ARG, moduloSelected)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater?.inflate(R.menu.menu_search_only, menu)
        val searchView = menu?.findItem(R.id.search_option)?.actionView as SearchView
        searchViewHandler.initialize(searchView)
    }

    override fun onSearchQuerySubmit(searchQuery: String) {
        showListFromResult(moduloViewModel.modulosOutcome.value)
    }

    override fun onSearchQueryCleared() {
        showListFromResult(moduloViewModel.modulosOutcome.value)
    }

    override fun showContentLayout() {
        recycler_view.visibility = View.VISIBLE
    }

    override fun hideContentLayout() {
        recycler_view.visibility = View.GONE
    }

    override fun onItemSelected(selectedItem: Any?) {
        val modulo = selectedItem as? Modulo
        val ticketFormFragment = getFragmentByTag(TicketFormFragment.TAG) as? TicketFormFragment
        ticketFormFragment?.setModulo(modulo)
        popBackStack()
    }

    companion object {
        const val TAG = "ModulosFragment"

        private const val MODULO_SELECTED_ARG = "MODULO_SELECTED_ARG"

        fun createNewInstance(modulo: Modulo?): ModulosFragment {
            val moduloFragment = ModulosFragment()
            val args = Bundle()

            args.putParcelable(MODULO_SELECTED_ARG, modulo)
            moduloFragment.arguments = args

            return moduloFragment
        }
    }
}