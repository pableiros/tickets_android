package com.pableiros.tickets.presentation.ticketform

import com.pableiros.tickets.R
import com.pableiros.tickets.view.BaseLoadingDialog

class TicketFormDialog: BaseLoadingDialog() {

    override fun getTitle(): String {
        return getString(R.string.creating)
    }

    override fun consume(useCache: Boolean) {
    }

    companion object {
        const val TAG = "TicketFormDialog"
    }
}