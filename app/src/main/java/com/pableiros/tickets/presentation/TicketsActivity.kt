package com.pableiros.tickets.presentation

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.Toolbar
import com.pableiros.tickets.R
import com.pableiros.tickets.common.DoAsync
import com.pableiros.tickets.common.network.NetworkStateReceiver
import com.pableiros.tickets.model.chain.TicketChain
import com.pableiros.tickets.model.local.TicketLocalData
import com.pableiros.tickets.presentation.ticketform.TicketFormFragment
import com.pableiros.tickets.presentation.tickets.TicketsFragment
import com.pableiros.tickets.view.BaseAppCompatActivity

class TicketsActivity : BaseAppCompatActivity(), NetworkStateReceiver.NetworkStateReceiverListener {

    override val contentId: Int
        get() = R.id.content

    private var networkStateReceiver: NetworkStateReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tickets)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)

        setSupportActionBar(toolbar)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.content, TicketsFragment(), TicketsFragment.TAG)
                .commit()
        }

        networkStateReceiver = NetworkStateReceiver(this)
        networkStateReceiver?.addListener(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            Snackbar.make(findViewById(contentId), R.string.ticket_success_created, Snackbar.LENGTH_LONG).show()

            val ticketsFragment = supportFragmentManager.findFragmentByTag(TicketsFragment.TAG) as? TicketsFragment
            ticketsFragment?.refreshList()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        networkStateReceiver?.removeListener(this)
        unregisterReceiver(networkStateReceiver)
    }

    override fun onNetworkAvailable() {
        DoAsync {
            val tickets = TicketLocalData.getPendingToSync()
            TicketChain.createChain(tickets)?.handle {  }
        }
    }

    override fun onNetworkUnavailable() {

    }
}