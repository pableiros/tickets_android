package com.pableiros.tickets.presentation.ticketform.modulos

import android.arch.lifecycle.LiveData
import com.pableiros.tickets.common.Outcome
import com.pableiros.tickets.common.toLiveData
import com.pableiros.tickets.model.entities.Modulo
import com.pableiros.tickets.model.repository.ModuloRepository
import com.pableiros.tickets.presentation.BaseListViewModel

class ModuloViewModel: BaseListViewModel() {

    val modulosOutcome: LiveData<Outcome<List<Modulo>>> by lazy {
        moduloRepository.outcome!!.toLiveData(compositeDisposable)
    }

    private val moduloRepository = ModuloRepository()

    fun fetch() {
        if (modulosOutcome.value == null) {
            moduloRepository.fetch()
        }
    }

    fun refresh() {
        moduloRepository.refresh()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}