package com.pableiros.tickets.presentation.tickets

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import com.pableiros.tickets.R
import com.pableiros.tickets.common.Outcome
import com.pableiros.tickets.common.reObserve
import com.pableiros.tickets.model.entities.Ticket
import com.pableiros.tickets.presentation.ticketdetalle.TicketDetalleActivity
import com.pableiros.tickets.presentation.ticketdetalle.TicketDetalleFragment
import com.pableiros.tickets.presentation.ticketform.TicketFormActivity
import com.pableiros.tickets.view.BaseListFragment
import com.pableiros.tickets.view.LoadingView
import com.pableiros.tickets.view.SearchViewHandler
import com.pableiros.tickets.view.SimpleDividerItemDecoration
import kotlinx.android.synthetic.main.fragment_tickets.*

class TicketsFragment : BaseListFragment(),
    LoadingView.LoadingViewInteractionListener,
    TicketRecyclerAdapter.TicketRecyclerAdapterInteractionListener,
    SearchViewHandler.SearchListener {

    private val ticketViewModel: TicketViewModel by lazy {
        val ticketViewModelFactory = TicketViewModelFactory()
        ViewModelProviders.of(this, ticketViewModelFactory).get(TicketViewModel::class.java)
    }

    private val ticketRecyclerAdapter: TicketRecyclerAdapter by lazy {
        TicketRecyclerAdapter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val appCompatActivity = activity as AppCompatActivity?
        appCompatActivity?.actionBar?.setDisplayHomeAsUpEnabled(false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setToolbarTitle(getString(R.string.tickets))
        setHasOptionsMenu(true)

        val view = inflater.inflate(R.layout.fragment_tickets, container, false)
        loadingView = view.findViewById(R.id.loading_view)
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        searchViewHandler = SearchViewHandler(this, this, swipeRefreshLayout, ticketViewModel.searchQuery)
        loadingView?.setInteractionListener(this)

        swipeRefreshLayout?.setOnRefreshListener {
            ticketViewModel.searchQuery.value = null
            ticketViewModel.refresh(false)
        }

        fab_open_ticket_form.setOnClickListener {
            val intent = Intent(context, TicketFormActivity::class.java)
            startActivityForResult(intent, TicketFormActivity.NEW_TICKET_REQUEST_CODE)
        }

        recycler_view.adapter = ticketRecyclerAdapter
        recycler_view.layoutManager = LinearLayoutManager(context)

        ticketViewModel.ticketOutcome.reObserve(this, ticketListObserver)
        ticketViewModel.fetch()
    }

    private val ticketListObserver = Observer<Outcome<List<Ticket>>> { outcome ->
        validateOutcomeFromResult(outcome, recycler_view)
    }

    fun refreshList() {
        ticketViewModel.refresh(true)
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T> showListFromResult(outcome: Outcome<List<T>>?) {
        if (outcome == null || outcome !is Outcome.Success) {
            return
        }

        setListToRecyclerView(outcome.data as List<Ticket>)
    }

    private fun setListToRecyclerView(ticketList: List<Ticket>) {
        val searchQuery = ticketViewModel.searchQuery.value

        ticketRecyclerAdapter.submitList(ticketList, searchQuery ?: "")

        context?.let {
            recycler_view.addItemDecoration(SimpleDividerItemDecoration(it))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater?.inflate(R.menu.menu_search_only, menu)
        val searchView = menu?.findItem(R.id.search_option)?.actionView as SearchView
        searchViewHandler.initialize(searchView)
    }

    override fun onSearchQuerySubmit(searchQuery: String) {
        showListFromResult(ticketViewModel.ticketOutcome.value)
    }

    override fun onSearchQueryCleared() {
        showListFromResult(ticketViewModel.ticketOutcome.value)
    }

    override fun showContentLayout() {
        recycler_view.visibility = View.VISIBLE
    }

    override fun hideContentLayout() {
        recycler_view.visibility = View.GONE
    }

    override fun onClick(ticket: Ticket) {
        val intent = Intent(context, TicketDetalleActivity::class.java)
        intent.putExtra(TicketDetalleActivity.TICKET_EXTRA, ticket)
        startActivity(intent)
    }

    companion object {
        const val TAG = "TicketsFragment"
    }
}