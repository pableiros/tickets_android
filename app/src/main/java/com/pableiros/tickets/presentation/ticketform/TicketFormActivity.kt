package com.pableiros.tickets.presentation.ticketform

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import com.pableiros.tickets.R
import com.pableiros.tickets.presentation.TicketsActivity
import com.pableiros.tickets.view.BaseAppCompatActivity
import com.pableiros.tickets.view.ToolbarBackButtonListener

class TicketFormActivity: BaseAppCompatActivity(),
    TicketFormFragment.TicketFormOnFragmentInteractionListener {

    override val contentId: Int
        get() = R.id.content

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ticket_form)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)

        setSupportActionBar(toolbar)

        toolbar.setNavigationOnClickListener(ToolbarBackButtonListener(this))

         if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.content, TicketFormFragment(), TicketFormFragment.TAG)
                    .commit()
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun handleTicketCreated() {
        val intent = Intent()
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    companion object {
        const val NEW_TICKET_REQUEST_CODE = 0
    }
}