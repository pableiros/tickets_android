package com.pableiros.tickets.presentation.ticketdetalle

import android.os.Bundle
import android.support.v7.widget.Toolbar
import com.pableiros.tickets.R
import com.pableiros.tickets.model.entities.Ticket
import com.pableiros.tickets.view.BaseAppCompatActivity
import com.pableiros.tickets.view.ToolbarBackButtonListener

class TicketDetalleActivity: BaseAppCompatActivity() {

    override val contentId: Int
        get() = R.id.content

     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ticket_detalle)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)

        setSupportActionBar(toolbar)

        toolbar.setNavigationOnClickListener(ToolbarBackButtonListener(this))

         if (savedInstanceState == null) {
             val ticket = intent.getParcelableExtra<Ticket>(TICKET_EXTRA)

            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.content, TicketDetalleFragment.createNewInstance(ticket), TicketDetalleFragment.TAG)
                    .commit()
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    companion object {
        const val TICKET_EXTRA = "TICKET_EXTRA"
    }
}