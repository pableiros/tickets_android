package com.pableiros.tickets.presentation.tickets

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

class TicketViewModelFactory: ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return TicketViewModel() as T
    }
}