package com.pableiros.tickets.common

sealed class NetworkError {
    data class RequestError(val httpCode: Int, val errorMessage: String): NetworkError()
    class ServerError: NetworkError()
    class ConnectionError: NetworkError()
}