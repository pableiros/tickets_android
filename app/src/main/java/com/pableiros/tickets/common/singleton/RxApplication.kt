package com.pableiros.tickets.common.singleton

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import android.support.v7.app.AppCompatDelegate
import android.util.LruCache
import com.github.ajalt.reprint.core.Reprint
import com.pableiros.tickets.model.retrofit.TicketNetworkService
import io.reactivex.Single
import java.lang.ref.WeakReference
import io.realm.Realm

class RxApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        mContext = WeakReference(this)
        setupRealm()
        Reprint.initialize(this)
        rxApplication = this
        networkService = TicketNetworkService()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    companion object {

        var isAppOnResume: Boolean = false

        private var rxApplication: RxApplication? = null
        private var networkService: TicketNetworkService? = null
        private var mContext: WeakReference<Context>? = null
        private val requestCache: LruCache<String, Single<*>> = LruCache(10)

        fun getNetworkService(): TicketNetworkService? {
            return networkService
        }

        fun getContext(): Context? {
            return mContext?.get()
        }

        fun getRequestCache(): LruCache<String, Single<*>> {
            return requestCache
        }
    }

    private fun setupRealm() {
        Realm.init(this)

    }
}
