package com.pableiros.tickets.common

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import java.io.IOException

open class ErrorHandler(protected var internetConnection: InternetConnection?) {

    fun handle(exception: Throwable): NetworkError {

        when {
            exception is HttpException -> {
                return try {
                    val httpCode = exception.code()

                    NetworkError.RequestError(httpCode, exception.message())

                } catch (e: Exception) {
                    e.printStackTrace()
                    NetworkError.ServerError()
                }
            }

            exception is IOException && internetConnection?.isConnected() == true -> {
                return NetworkError.ServerError()
            }

            exception is IOException && internetConnection?.isConnected() == false -> {
                return NetworkError.ConnectionError()
            }
        }

        return NetworkError.ServerError()
    }
}