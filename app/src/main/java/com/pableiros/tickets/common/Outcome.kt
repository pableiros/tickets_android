package com.pableiros.tickets.common

sealed class Outcome<T> {
    data class Progress<T>(var isFirstLoad: Boolean) : Outcome<T>()
    data class Success<T>(var data: T) : Outcome<T>()
    data class Failure<T>(val e: NetworkError) : Outcome<T>()
    data class Clear<T>(var isClear: Boolean) : Outcome<T>()

    companion object {
        fun <T> loading(isFirstLoad: Boolean): Outcome<T> = Progress(isFirstLoad)

        fun <T> success(data: T): Outcome<T> = Success(data)

        fun <T> failure(e: NetworkError): Outcome<T> = Failure(e)

        fun <T> clear(isCleared: Boolean): Outcome<T> = Clear(isCleared)
    }
}