package com.pableiros.tickets.common;

import java.text.DateFormat;
import java.util.Calendar;

public class DateTools {

    public static String apiDateToFormattedDate(String apiDate){
        String[] dateStrArray = apiDate.split("-");
        int year = Integer.valueOf(dateStrArray[0]);
        int month = Integer.valueOf(dateStrArray[1]) - 1;
        int day = Integer.valueOf(dateStrArray[2]);

        return getFormattedDate(year, month, day);
    }

    private static String getFormattedDate(int year, int month, int day){
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return DateFormat.getDateInstance().format(calendar.getTime());
    }
}
