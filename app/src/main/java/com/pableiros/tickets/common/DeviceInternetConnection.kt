package com.pableiros.tickets.common

import android.content.Context
import android.net.ConnectivityManager

class DeviceInternetConnection(private val context: Context): InternetConnection {

    override fun isConnected(): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }
}