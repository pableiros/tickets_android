package com.pableiros.tickets.common

interface InternetConnection {

    fun isConnected(): Boolean

}