package com.pableiros.tickets.common.network

import android.support.v4.util.LruCache
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


open class NetworkService <out T> (
    baseUrl: String,
    service : Class<T>,
    customInterceptors: List<Interceptor>?,
    private val timeOut : Long = 300,
    cacheSize : Int = 10
) {

    private val apiObservables: LruCache<Class<*>, Observable<*>> = LruCache(cacheSize)

    val httpClient = createOkHttpClient(customInterceptors)

    private val retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(httpClient)
        .build()

    val retrofitApi : T = retrofit.create(service)

    private fun createOkHttpClient(customInterceptors: List<Interceptor>?): OkHttpClient {
        val httpClientBuilder = OkHttpClient.Builder()
            .connectTimeout(timeOut, TimeUnit.SECONDS)
            .readTimeout(timeOut, TimeUnit.SECONDS)
            .writeTimeout(timeOut, TimeUnit.SECONDS)

        addCustomOkHttpInterceptors(httpClientBuilder, customInterceptors)
        addLoggingOkHttpInterceptor(httpClientBuilder)

        return httpClientBuilder.build()
    }

    private fun addLoggingOkHttpInterceptor(httpClientBuilder: OkHttpClient.Builder) {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        httpClientBuilder.addInterceptor(logging)
    }

    private fun addCustomOkHttpInterceptors(httpClientBuilder: OkHttpClient.Builder, customInterceptors: List<Interceptor>?) {
        customInterceptors?.let {
            it.forEach { interceptor ->
                httpClientBuilder.addInterceptor(interceptor)
            }
        }
    }

    fun clearCache() {
        apiObservables.evictAll()
    }

    fun getPreparedObservable(unPreparedObservable: Observable<*>, clazz: Class<*>, cacheObservable: Boolean, useCache: Boolean): Observable<*> {

        var preparedObservable: Observable<*>? = null

        if (useCache) {
            preparedObservable = apiObservables.get(clazz)
        } else {
            apiObservables.remove(clazz)
        }

        if (preparedObservable != null) {
            return preparedObservable
        }

        preparedObservable = unPreparedObservable.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())

        if (cacheObservable) {
            preparedObservable = preparedObservable!!.cache()
            apiObservables.put(clazz, preparedObservable!!)
        }

        return preparedObservable
    }

    fun popApiObservable(clazz: Class<*>) {
        apiObservables.remove(clazz)
    }
}
