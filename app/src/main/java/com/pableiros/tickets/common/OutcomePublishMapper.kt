package com.pableiros.tickets.common

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

fun <T> PublishSubject<T>.toLiveData(compositeDisposable: CompositeDisposable): LiveData<T> {
    val data = MutableLiveData<T>()
    compositeDisposable.add(this.subscribe { t: T -> data.value = t })
    return data
}

fun <T> PublishSubject<Outcome<T>>.failed(e: NetworkError) {
    with(this){
        onNext(Outcome.failure(e))
    }
}

fun <T> PublishSubject<Outcome<T>>.success(t: T) {
    with(this){
        onNext(Outcome.success(t))
    }
}

fun <T> PublishSubject<Outcome<T>>.loading(isFirstLoad: Boolean) {
    this.onNext(Outcome.loading(isFirstLoad))
}

fun <T> PublishSubject<Outcome<T>>.clear() {
    this.onNext(Outcome.clear(true))
}